<?php

namespace Drupal\entity_view_mode_path\Repository;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class EntityviewmodepathPatternRepository.
 *
 * @package Drupal\entity_view_mode_path\Repository
 */
class EntityviewmodepathPatternRepository {
  /**
   * The entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * EntityviewmodepathPatternRepository constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager interface.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Find all patterns.
   *
   * @return \Drupal\entity_view_mode_path\EntityviewmodepathPatternInterface[]
   *   The entityviewmodepath pattern interface.
   */
  public function findAll() {
    static $patterns;

    if (!isset($patterns)) {
      $ids      = $this->entityTypeManager->getStorage('entity_view_mode_path_pattern')->getQuery()->sort('weight')->execute();
      $patterns = $this->entityTypeManager->getStorage('entity_view_mode_path_pattern')->loadMultiple($ids);
    }

    return $patterns;
  }

}
